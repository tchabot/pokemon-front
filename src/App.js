import React from "react";
import "./App.css";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import BoxList from "./components/boxes/BoxList";
import PageNotFound from "./components/PageNotFound";
import PokemonList from "./components/pokemons/PokemonList";
import PokemonTypesList from "./components/pokemon-types/PokemonTypesList";
import Login from "./components/account/Login";
import Register from "./components/account/Register";
import PokemonAdd from "./components/pokemons/PokemonAdd";
import PokemonUpdate from "./components/pokemons/PokemonUpdate";
import BoxUpdate from "./components/boxes/BoxUpdate";
import BoxAdd from "./components/boxes/BoxAdd";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <PrivateRoute exact path="/boxes">
          <BoxList />
        </PrivateRoute>
        <PrivateRoute exact path="/boxes/add">
          <BoxAdd />
        </PrivateRoute>
        <PrivateRoute exact path="/boxes/:id">
          <PokemonList />
        </PrivateRoute>
        <PrivateRoute exact path="/boxes/:id/update">
          <BoxUpdate />
        </PrivateRoute>
        <PrivateRoute exact path="/pokemon-types">
          <PokemonTypesList />
        </PrivateRoute>
        <PrivateRoute exact path="/pokemons/add">
          <PokemonAdd />
        </PrivateRoute>
        <PrivateRoute exact path="/pokemons/:id">
          <PokemonUpdate />
        </PrivateRoute>
        <Route path="*">
          <PageNotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;

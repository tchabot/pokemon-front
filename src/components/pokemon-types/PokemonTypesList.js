import React, { useEffect } from "react";
import MaterialTable from "material-table";
import API from "../../api";
import Menu from "../Menu";
import Container from "@material-ui/core/Container";

export default function MaterialTableDemo() {
  const [state, setState] = React.useState({
    columns: [{ title: "Nom", field: "name" }],
    data: [],
  });
  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };

  useEffect(() => {
    loadTypes();
  }, []);

  const loadTypes = () => {
    API.get("pokemon-types", {headers})
      .then((result) => {
        setState((prevState) => {
          return { ...prevState, data: result.data };
        });
      })
      .catch((error) => console.log(error));
  };

  const addType = (type) => {
    API.post("pokemon-types", type, {headers})
      .then()
      .catch((error) => console.log(error));
  };

  const updateType = (typeId, data) => {
    API.put("pokemon-types/" + typeId, data, {headers})
      .then()
      .catch((error) => console.log(error));
  };

  const deleteType = (typeId) => {
    API.delete("pokemon-types/" + typeId, {headers})
      .then()
      .catch((error) => console.log(error));
  };

  return (
    <div>
      <Menu />
      <Container>
        <MaterialTable
          title="Types de pokémons"
          columns={state.columns}
          data={state.data}
          localization={{
            pagination: {
              labelDisplayedRows: "{from}-{to} de {count}",
            },
            toolbar: {
              nRowsSelected: "{0} ligne(s) sélectionnée(s)",
              searchPlaceholder: "Rechercher",
              searchTooltip: "Rechercher",
            },
            header: {
              actions: "Actions",
            },
            body: {
              emptyDataSourceMessage: "Aucun enregistrement",
              filterRow: {
                filterTooltip: "Filtre",
              },
            },
          }}
          editable={{
            onRowAdd: (newData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  addType(newData);
                  resolve();
                  setState((prevState) => {
                    const data = [...prevState.data];
                    data.push(newData);
                    return { ...prevState, data };
                  });
                }, 600);
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  updateType(newData.id, newData);
                  resolve();
                  if (oldData) {
                    setState((prevState) => {
                      const data = [...prevState.data];
                      data[data.indexOf(oldData)] = newData;
                      return { ...prevState, data };
                    });
                  }
                }, 600);
              }),
            onRowDelete: (oldData) =>
              new Promise((resolve) => {
                setTimeout(() => {
                  deleteType(oldData.id);
                  resolve();
                  setState((prevState) => {
                    const data = [...prevState.data];
                    data.splice(data.indexOf(oldData), 1);
                    return { ...prevState, data };
                  });
                }, 600);
              }),
          }}
        />
      </Container>
    </div>
  );
}

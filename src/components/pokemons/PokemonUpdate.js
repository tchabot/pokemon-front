import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import API from "../../api";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory, useParams } from "react-router-dom";
import { Container } from "@material-ui/core";
import Menu from "../Menu";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    width: "100%",
  },
}));

const styles = {
  alert: {
    marginBottom: 20,
  },
};

export default function PokemonUpdate() {
  const classes = useStyles();
  const history = useHistory();

  const [types, setTypes] = useState([]);
  const [error, setError] = useState(false);
  const [boxes, setBoxes] = useState([]);
  const [pokemon, setPokemon] = useState({
    name: "",
    pokemonType: { id: null },
    box: { id: null },
  });

  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };

  const closeAlert = () => {
    setError(false);
  };

  let urlParams = useParams();

  const handleSubmitPokemon = () => {
    API.post("pokemons", pokemon, { headers })
      .then(() => {
        history.push("/boxes");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    API.get("pokemons/" + urlParams.id, { headers })
      .then((result) => {
        setPokemon(result.data);
      })
      .catch((error) => {
        console.log(error);
      });
    API.get("boxes", { headers })
      .then((result) => {
        setBoxes(result.data);
      })
      .catch((error) => {
        console.log(error);
      });
    API.get("pokemon-types", { headers })
      .then((result) => {
        setTypes(result.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div>
      <Menu />
      <Container>
        <h1>Modifier le pokémon</h1>
        {error && (
          <Alert onClose={closeAlert} style={styles.alert} severity="error">
            {error}
          </Alert>
        )}
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Nom"
          type="text"
          value={pokemon.name}
          onChange={(e) => setPokemon({ ...pokemon, name: e.target.value })}
          fullWidth
        />
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="pokemon-type-label">Type</InputLabel>
          <Select
            labelId="pokemon-type-label"
            id="pokemon-type"
            value={pokemon.pokemonType.id}
            onChange={(e) =>
              setPokemon({ ...pokemon, pokemonType: { id: e.target.value } })
            }
            label="Type"
            name="pokemonType"
          >
            {types.map((type) => {
              return (
                <MenuItem key={type.id} value={type.id}>
                  {type.name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="box-label">Boîte</InputLabel>
          <Select
            labelId="box-label"
            id="box-select"
            value={pokemon.box.id}
            onChange={(e) =>
              setPokemon({ ...pokemon, box: { id: e.target.value } })
            }
            label="Boîte"
            name="box"
          >
            {boxes.map((box) => {
              return (
                <MenuItem key={box.id} value={box.id}>
                  {box.name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <Button
          onClick={() => history.push("/boxes/" + pokemon.box.id)}
          color="primary"
        >
          Annuler
        </Button>
        <Button onClick={handleSubmitPokemon} color="primary">
          Enregistrer
        </Button>
      </Container>
    </div>
  );
}

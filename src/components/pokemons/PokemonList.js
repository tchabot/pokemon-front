import React, { useState, useEffect } from "react";
import { Container, Grid } from "@material-ui/core";
import Menu from "../Menu";
import { useParams } from "react-router-dom";
import PokemonItem from "./PokemonItem";
import API from "../../api";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Alert from "@material-ui/lab/Alert";
import { useHistory } from "react-router-dom";

export default function PokemonList() {
  const [box, setBox] = useState(null);
  const [pokemons, setPokemons] = useState([]);
  const [pokemonTypes, setPokemonTypes] = useState([]);
  const [boxes, setBoxes] = useState([]);
  const [error, setError] = useState(null);
  const history = useHistory();

  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };

  const styles = {
    floatingButton: {
      margin: "0 0 40px 0",
      top: "auto",
      right: 30,
      bottom: 15,
      left: "auto",
      position: "fixed",
      backgroundColor: "#C99D40",
    },
    alert: {
      marginBottom: 20,
    },
  };

  let urlParams = useParams();

  const closeAlert = () => {
    setError(false);
  };

  useEffect(() => {
    API.get("boxes/" + urlParams.id, { headers })
      .then((result) => {
        setPokemons(result.data.pokemons);
        setBox(result.data);
      })
      .catch((error) => {
        console.log(error.response);
      });
    API.get("pokemon-types", { headers })
      .then((result) => {
        setPokemonTypes(result.data);
      })
      .catch((error) => {
        console.log(error);
      });
    API.get("boxes", { headers })
      .then((result) => {
        setBoxes(result.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <div>
      <Menu />
      <Container>
        <h1>Pokémons de la boîte {box && box.name}</h1>
        {error && (
          <Alert onClose={closeAlert} style={styles.alert} severity="error">
            {error}
          </Alert>
        )}
        <Grid container spacing={2}>
          {pokemons &&
            pokemons.map((pokemon) => {
              return (
                <Grid item key={pokemon.id}>
                  <PokemonItem
                    key={pokemon.id}
                    pokemon={pokemon}
                    box={box}
                    pokemonTypes={pokemonTypes}
                    boxes={boxes}
                  />
                </Grid>
              );
            })}
        </Grid>
        <Fab
          onClick={() => history.push("/pokemons/add")}
          size="medium"
          style={styles.floatingButton}
          aria-label="add"
        >
          <AddIcon />
        </Fab>
      </Container>
    </div>
  );
}

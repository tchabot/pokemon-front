import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import API from "../../api";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
  },
  pos: {
    marginBottom: 12,
  },
  chip: {
    marginTop: 15,
  },
  formControl: {
    margin: theme.spacing(1),
    width: "100%",
  },
}));

export default function PokemonItem(props) {
  const classes = useStyles();
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };
  const history = useHistory();

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false);
  };

  const handleDeletePokemon = () => {
    handleCloseDeleteDialog();

    API.delete("pokemons/" + props.pokemon.id, { headers })
      .then(() => {
        history.push("/boxes");
      })
      .catch((error) => console.log(error));
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {props.pokemon.name}
        </Typography>
        <Chip
          key={props.pokemon.pokemonType.id}
          label={props.pokemon.pokemonType.name}
          className={classes.chip}
        />
      </CardContent>
      <CardActions>
        <IconButton
          aria-label="edit"
          onClick={() => history.push("/pokemons/" + props.pokemon.id)}
        >
          <EditIcon />
        </IconButton>
        <IconButton aria-label="delete" onClick={handleOpenDeleteDialog}>
          <DeleteIcon />
        </IconButton>
      </CardActions>
      <Dialog
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Êtes-vous sûr de vouloir supprimer ce pokémon ?
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            Non
          </Button>
          <Button onClick={handleDeletePokemon} color="primary" autoFocus>
            Oui
          </Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
}

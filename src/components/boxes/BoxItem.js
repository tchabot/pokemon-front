import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import API from "../../api";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  pos: {
    marginBottom: 12,
  },
  chip: {
    marginTop: 20,
  },
});

export default function BoxItem(props) {
  let history = useHistory();
  const [pokemons, setPokemons] = useState(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };
  let pokemonTypes;

  if (pokemons) {
    pokemonTypes = [
      ...new Set(pokemons.map((pokemon) => pokemon.pokemonType.name)),
    ];
  }

  const classes = useStyles();

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false);
  };

  const deleteBox = () => {
    API.delete("boxes/" + props.box.id, { headers })
      .then(() => {
        props.loadBoxes();
      })
      .catch((error) => console.log(error.response));
  };

  useEffect(() => {
    API.get("boxes/" + props.box.id, {
      headers,
    })
      .then((result) => {
        setPokemons(result.data.pokemons);
      })
      .catch((err) => {
        console.log(err.response);
      });
  }, []);

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {props.box.name}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          --------------------
        </Typography>
        {pokemons && (
          <Typography variant="body2" component="p">
            <b>{pokemons.length}</b> pokémons à l'intérieur
          </Typography>
        )}
        {pokemonTypes &&
          pokemonTypes.map((type) => {
            return <Chip key={type} className={classes.chip} label={type} />;
          })}
      </CardContent>
      <CardActions>
        <Button
          size="small"
          onClick={() => history.push("boxes/" + props.box.id)}
        >
          Ouvrir
        </Button>
        <IconButton
          aria-label="edit"
          onClick={() => history.push("boxes/" + props.box.id + "/update")}
        >
          <EditIcon />
        </IconButton>
        <IconButton aria-label="delete" onClick={handleOpenDeleteDialog}>
          <DeleteIcon />
        </IconButton>
      </CardActions>
      <Dialog
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Êtes-vous sûr de vouloir supprimer cette boîte ?
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            Non
          </Button>
          <Button onClick={deleteBox} color="primary" autoFocus>
            Oui
          </Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
}

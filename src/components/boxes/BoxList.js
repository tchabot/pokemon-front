import React, { useState, useEffect } from "react";
import Menu from "../Menu";
import { Container, Grid } from "@material-ui/core";
import BoxItem from "./BoxItem";
import API from "../../api";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

export default function BoxList() {
  const [boxes, setBoxes] = useState([]);
  const [box, setBox] = useState({ id: null, name: "" });
  const [open, setOpen] = React.useState(false);
  const history = useHistory();

  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };

  const styles = {
    floatingButton: {
      margin: "0 0 40px 0",
      top: "auto",
      right: 30,
      bottom: 15,
      left: "auto",
      position: "fixed",
      backgroundColor: "#C99D40",
    },
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const loadBoxes = () => {
    API.get("boxes", {
      headers,
    })
      .then((result) => {
        setBoxes(result.data);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };

  const addBox = () => {
    if (box.id) {
      API.put("boxes/" + box.id, box, { headers })
        .then(() => {
          setOpen(false);
          loadBoxes();
        })
        .catch((err) => {
          console.log(err.response);
        });
    } else {
      API.post("boxes", box, { headers })
        .then(() => {
          setOpen(false);
          loadBoxes();
        })
        .catch((err) => {
          console.log(err.response);
        });
    }
  };

  const handleBox = (newBox) => {
    setOpen(true);
    setBox({ id: newBox.id, name: newBox.name });
  };

  useEffect(() => {
    loadBoxes();
  }, []);

  if (!boxes) {
    return <div></div>;
  }

  return (
    <div>
      <Menu />
      <Container>
        <h1>Mes boîtes</h1>
        <Grid container spacing={2}>
          {boxes &&
            boxes.map((box) => {
              return (
                <Grid item key={box.id}>
                  <BoxItem
                    loadBoxes={loadBoxes}
                    handleEditBox={handleClickOpen}
                    key={box.id}
                    box={box}
                    handleBox={handleBox}
                  />
                </Grid>
              );
            })}
        </Grid>
        <Fab
          onClick={() => history.push("boxes/add")}
          size="medium"
          style={styles.floatingButton}
          aria-label="add"
        >
          <AddIcon />
        </Fab>
      </Container>
    </div>
  );
}

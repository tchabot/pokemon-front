import React, { useState, useEffect } from "react";
import { Container } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import API from "../../api";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Menu from "../Menu";

export default function BoxAdd() {
  const [box, setBox] = useState({ name: "" });
  const [user, setUser] = useState({});

  let history = useHistory();

  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };

  useEffect(() => {
    API.get("profile", {
      headers,
    })
      .then((result) => {
        setUser(result.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const addBox = () => {
    API.post("boxes", { ...box, user: user }, { headers })
      .then(() => {
        history.push("/boxes");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <Container>
        <Menu />
        <div>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Nom de la boîte"
            type="text"
            value={box.name}
            onChange={(e) => setBox({ name: e.target.value })}
            fullWidth
          />
          <Button onClick={() => history.push("/boxes")} color="primary">
            Annuler
          </Button>
          <Button onClick={addBox} color="primary">
            Enregistrer
          </Button>
        </div>
      </Container>
    </div>
  );
}

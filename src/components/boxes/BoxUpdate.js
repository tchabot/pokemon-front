import React, { useState, useEffect } from "react";
import { Container } from "@material-ui/core";
import { useParams, useHistory } from "react-router-dom";
import API from "../../api";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Menu from "../Menu";

export default function BoxUpdate() {
  const [box, setBox] = useState({ name: "" });

  let urlParams = useParams();
  let history = useHistory();

  const headers = {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  };

  useEffect(() => {
    API.get("boxes/" + urlParams.id, {
      headers,
    })
      .then((result) => {
        setBox(result.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const updateBox = () => {
    API.put("boxes/" + urlParams.id, box, { headers })
      .then(() => {
        history.push("/boxes");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <Container>
        <Menu />
        <div>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Nom de la boîte"
            type="text"
            value={box.name}
            onChange={(e) => setBox({ ...box, name: e.target.value })}
            fullWidth
          />
          <Button onClick={() => history.push("/boxes")} color="primary">
            Annuler
          </Button>
          <Button onClick={updateBox} color="primary">
            Enregistrer
          </Button>
        </div>
      </Container>
    </div>
  );
}
